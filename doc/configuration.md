# Configuration & Settings

All configuration is done using environment variables.
The term *settings* is used when referring to the content of the downloaded JSON document which describes how mails are actually fetched and sent.

The only required configuration is:

* `APP_FETCH_URL` being the HTTP(S) URL to the JSON document containing the settings

Additionally these configurations are available:

* `APP_INTERVAL` (default: `10m`) at which time interval settings will be downloaded to then fetch and send mails
* `APP_UNADJUSTED_AGE` (default: `6h`) being the maximum age of an email before its date will overwritten with the current time

For debugging issues with processing mails this configuration is also available:

* `APP_WRITE_MESSAGES` (default: `false`) will write all fetched and modified messages to disk

All other environment variables and how *settings* work are documented in the following sections.

## HTTP Request

It's possible to configure a few details on how the HTTP request is performed using these environment variables:

* `APP_FETCH_METHOD` (default: `GET`) to use a different HTTP method (e.g. `POST`)
* `APP_FETCH_AUTHORIZATION` (default: none) to set the value of the `Authorization` header

## HTTP Response

The data returned by the HTTP call must eventually look like this:
```json
[
    {
        "imap_server": "imap.example.com",
        "imap_username": "list@example.com",
        "imap_password": "password",
        "sender": "list@example.com",
        "recipients": [
            "recipient1@example.org",
            "recipient2@other-domain.net"
        ],
        "smtp_server": "smtp.example.com",
        "smtp_username": "list@example.com",
        "smtp_password": "password"
    }
]
```

The received data can be *preprocessed* using JSON pointers ([RFC 6901](https://datatracker.ietf.org/doc/html/rfc6901)).
These are set via additional environment variables:

* `APP_LIST_POINTER` (default: none) to the array of the objects to process

* `APP_IMAP_SERVER_POINTER` (default: `imap_server`) to the string containing the IMAP server's address
* `APP_IMAP_PORT_POINTER` (default: `imap_port`) to the number containing the IMAP server's port (default: `993`)
* `APP_IMAP_USERNAME_POINTER` (default: `imap_username`) to the string containing the username to authenticate with at the IMAP server
* `APP_IMAP_PASSWORD_POINTER` (default: `imap_password`) to the string containing the password to authenticate with at the IMAP server

* `APP_SMTP_SERVER_POINTER` (default: `smtp_server`) to the string containing the SMTP server's address
* `APP_SMTP_PORT_POINTER` (default: `smtp_port`) to the number containing the SMTP server's port (default: `465`)
* `APP_SMTP_USERNAME_POINTER` (default: `smtp_username`) to the string containing the username to authenticate with at the SMTP server
* `APP_SMTP_PASSWORD_POINTER` (default: `smtp_password`) to the string containing the password to authenticate with at the SMTP server

* `APP_SENDER_POINTER` (default: `sender`) to the string containing the mail address used as the sender of the sent mails
* `APP_RECIPIENTS_POINTER` (default: `recipients`) to the array containing the list of recipients to send the mails to
* `APP_AUTO_REPLY_RECIPIENTS_POINTER` (default: `auto_reply_recipients`) to the array containing the list of recipients to send the automatic replies to
* `APP_RECIPIENT_POINTER` (default: none) to the string containing the recipient's mail address in case the array is of objects


### Example

Assuming the HTTP call returns:
```json
{
    "data": [
        {
            "imap_server": "imap.example.com",
            "imap_username": "list@example.com",
            "imap_password": "password",
            "sender": "list@example.com",
            "recipients": [
                {
                    "address": "recipient1@example.org"
                },
                {
                    "address": "recipient2@other-domain.net"
                }
            ],
            "smtp_server": "smtp.example.com",
            "smtp_username": "list@example.com",
            "smtp_password": "password"
        }
    ]
}
```

Then this data could be put into the required format by setting this configuration:

* `APP_LIST_POINTER` = `/data`
* `APP_RECIPIENT_POINTER` = `/address`
