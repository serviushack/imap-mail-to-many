# Directus Setup

The choice of HTTP calls to obtain JSON documents that describe the behavior might seem odd.
The whole reason for that is to support use cases like the setup described here.
Instead of having to provide a custom UI a different tool is used to manage the data.
Specifically [Directus](https://directus.io/) here.
Everything is prepared to quickly get started with this tool.

Directus itself can be started using:

```sh
cd dev
docker compose --profile dynamic up -d
```

Login at http://localhost:8055 using the credentials from the `docker-compose.yml` file.
Access your profile at the very bottom left of the page on desktop mode.
Add a token at the bottom of the page to setup the database.
Then apply the schema:

```sh
echo "User's token in Directus:"
read TOKEN
curl -H "Authorization: Bearer $TOKEN" --json @directus-schema.json -o diff.json http://localhost:8055/schema/diff
jq .data diff.json | curl -H "Authorization: Bearer $TOKEN" --json @- http://localhost:8055/schema/apply
```

Some test data can also be loaded using:

```sh
curl -H "Authorization: Bearer $TOKEN" --json @directus-data.json http://localhost:8055/items/Mailboxes
```

The necessary configuration for the application is:

```sh
export APP_FETCH_URL=http://localhost:8055/items/Mailboxes
export APP_FETCH_AUTHORIZATION="Bearer $TOKEN"
export APP_LIST_POINTER=/data
export APP_RECIPIENT_POINTER=/address
```
