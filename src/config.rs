use std::fmt;
use std::str::FromStr;
use std::time::Duration;

use anyhow::{Context, Result};
use duration_string::DurationString;
use reqwest::header::HeaderValue;
use reqwest::{Method, Url};
use serde::de::{self, Deserializer, Visitor};
use serde_inline_default::serde_inline_default;

struct ParserVisitor<T> {
    _phantom: std::marker::PhantomData<T>,
}

impl<T> Visitor<'_> for ParserVisitor<T>
where
    T: FromStr,
    T::Err: fmt::Display,
{
    type Value = T;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        value.parse::<T>().map_err(|e| E::custom(e))
    }
}

fn deserialize_with_parse<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
    T: FromStr,
    T::Err: fmt::Display,
{
    deserializer.deserialize_str(ParserVisitor {
        _phantom: Default::default(),
    })
}

fn deserialize_with_optional_parse<'de, D, T>(deserializer: D) -> Result<Option<T>, D::Error>
where
    D: Deserializer<'de>,
    T: FromStr,
    T::Err: fmt::Display,
{
    deserializer
        .deserialize_str(ParserVisitor {
            _phantom: Default::default(),
        })
        .map(|v| Some(v))
}

struct DurationVisitor;

impl Visitor<'_> for DurationVisitor {
    type Value = Duration;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        DurationString::from_str(value)
            .map(|v| v.into())
            .map_err(|e| E::custom(e))
    }
}

fn deserialize_duration<'de, D>(deserializer: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_str(DurationVisitor)
}

#[serde_inline_default]
#[derive(Debug, serde::Deserialize)]
pub struct Config {
    #[serde(deserialize_with = "deserialize_with_parse")]
    pub fetch_url: Url,
    #[serde(deserialize_with = "deserialize_with_parse")]
    #[serde_inline_default(Method::GET)]
    pub fetch_method: Method,
    #[serde(deserialize_with = "deserialize_with_optional_parse", default)]
    pub fetch_authorization: Option<HeaderValue>,
    #[serde(default)]
    pub list_pointer: String,
    #[serde_inline_default("/imap_server".to_owned())]
    pub imap_server_pointer: String,
    #[serde_inline_default("/imap_port".to_owned())]
    pub imap_port_pointer: String,
    #[serde_inline_default("/imap_username".to_owned())]
    pub imap_username_pointer: String,
    #[serde_inline_default("/imap_password".to_owned())]
    pub imap_password_pointer: String,
    #[serde_inline_default("/smtp_server".to_owned())]
    pub smtp_server_pointer: String,
    #[serde_inline_default("/smtp_port".to_owned())]
    pub smtp_port_pointer: String,
    #[serde_inline_default("/smtp_username".to_owned())]
    pub smtp_username_pointer: String,
    #[serde_inline_default("/smtp_password".to_owned())]
    pub smtp_password_pointer: String,
    #[serde_inline_default("/sender".to_owned())]
    pub sender_pointer: String,
    #[serde_inline_default("/recipients".to_owned())]
    pub recipients_pointer: String,
    #[serde_inline_default("/auto_reply_recipients".to_owned())]
    pub auto_reply_recipients_pointer: String,
    #[serde(default)]
    pub recipient_pointer: Option<String>,
    #[serde(default)]
    pub write_messages: bool,
    #[serde(deserialize_with = "deserialize_duration")]
    #[serde_inline_default(Duration::from_secs(10*60))]
    pub interval: Duration,
    #[serde_inline_default(Duration::from_secs(6*60*60))]
    pub unadjusted_age: Duration,
}

/// Read the configuration from environment variables.
pub fn read() -> Result<Config> {
    let config = config::Config::builder()
        .add_source(config::Environment::with_prefix("APP").try_parsing(true))
        .build()?;

    config
        .try_deserialize()
        .with_context(|| "Missing required configuration")
}
