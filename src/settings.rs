use crate::config::Config;
use anyhow::{Context, Result};

fn extract_by_pointer<T>(value: &mut serde_json::Value, pointer: &str) -> Result<T>
where
    T: serde::de::DeserializeOwned,
{
    value
        .pointer_mut(pointer)
        .map(|v| v.take())
        .ok_or_else(|| anyhow::format_err!("{} points to nothing", pointer))
        .and_then(|v| serde_json::from_value(v).map_err(anyhow::Error::from))
}

fn optional_extract_by_pointer<T>(value: &mut serde_json::Value, pointer: &str) -> Result<Option<T>>
where
    T: serde::de::DeserializeOwned,
{
    value
        .pointer_mut(pointer)
        .map(|v| v.take())
        .map(|v| serde_json::from_value::<Option<T>>(v).map_err(anyhow::Error::from))
        .transpose()
        .map(|v| v.flatten())
}

#[derive(Debug, PartialEq, Eq)]
pub struct Settings {
    pub imap_server: String,
    pub imap_port: u16,
    pub imap_username: String,
    pub imap_password: String,

    pub smtp_server: String,
    pub smtp_port: u16,
    pub smtp_username: Option<String>,
    pub smtp_password: Option<String>,

    pub sender: String,
    pub recipients: Vec<String>,
    pub auto_reply_recipients: Vec<String>,
}

fn extract_list_items_by_pointer(
    mut value: serde_json::Value,
    pointer: &Option<String>,
) -> Result<Vec<String>> {
    match pointer {
        Some(pointer) => {
            if !value.is_array() {
                anyhow::bail!("{} didn't point to an array", pointer)
            }

            let mut output = Vec::new();
            for (index, object) in value.as_array_mut().unwrap().iter_mut().enumerate() {
                output.push(
                    extract_by_pointer(object, pointer)
                        .with_context(|| format!("Processing recipients row {index}: {object}"))?,
                );
            }
            Ok(output)
        }
        None => Ok(serde_json::from_value(value)?),
    }
}

impl Settings {
    fn from(object: &mut serde_json::Value, config: &Config) -> Result<Self> {
        let imap_server = extract_by_pointer(object, &config.imap_server_pointer)?;
        let imap_port =
            optional_extract_by_pointer(object, &config.imap_port_pointer)?.unwrap_or(993);
        let imap_username = extract_by_pointer(object, &config.imap_username_pointer)?;
        let imap_password = extract_by_pointer(object, &config.imap_password_pointer)?;
        let smtp_server = extract_by_pointer(object, &config.smtp_server_pointer)?;
        let smtp_port =
            optional_extract_by_pointer(object, &config.smtp_port_pointer)?.unwrap_or(465);
        let smtp_username = optional_extract_by_pointer(object, &config.smtp_username_pointer)?;
        let smtp_password = optional_extract_by_pointer(object, &config.smtp_password_pointer)?;
        let sender = extract_by_pointer(object, &config.sender_pointer)?;
        let recipients: Vec<String> = extract_list_items_by_pointer(
            extract_by_pointer(object, &config.recipients_pointer)?,
            &config.recipient_pointer,
        )?;
        let auto_reply_recipients: Vec<String> = extract_list_items_by_pointer(
            extract_by_pointer(object, &config.auto_reply_recipients_pointer)?,
            &config.recipient_pointer,
        )?;

        Ok(Settings {
            imap_server,
            imap_port,
            imap_username,
            imap_password,
            smtp_server,
            smtp_port,
            smtp_username,
            smtp_password,
            sender,
            recipients,
            auto_reply_recipients,
        })
    }
}

/// Read the settings from a generic JSON value using the configured JSON pointers.
pub fn from(json_value: &mut serde_json::Value, config: &Config) -> Result<Vec<Settings>> {
    let mut json_list: serde_json::Value = extract_by_pointer(json_value, &config.list_pointer)?;

    if !json_list.is_array() {
        anyhow::bail!("{} didn't point to an array", config.list_pointer)
    }

    let mut settings = Vec::new();
    for (index, object) in json_list.as_array_mut().unwrap().iter_mut().enumerate() {
        settings.push(
            Settings::from(object, config)
                .with_context(|| format!("Processing settings row {index}: {object}"))?,
        );
    }
    Ok(settings)
}
