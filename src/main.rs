mod config;
mod settings;

use std::fs::File;
use std::io::prelude::*;
use std::net::TcpStream;
use std::str::from_utf8;

use anyhow::{Context, Result};
use log::{debug, error};
use reqwest::header::{ACCEPT, AUTHORIZATION};
use reqwest::Client;
use rustls_connector::RustlsConnector;
use tokio::time::{self, MissedTickBehavior};

const DATE_HEADER: &str = "Date: ";

async fn run(config: &config::Config) -> Result<()> {
    let mut request = Client::new()
        .request(config.fetch_method.clone(), config.fetch_url.clone())
        .header(ACCEPT, "application/json");
    if let Some(ref value) = config.fetch_authorization {
        request = request.header(AUTHORIZATION, value);
    }
    let response = request.send().await?;

    let mut json_value: serde_json::Value = serde_json::from_slice(&response.bytes().await?)
        .with_context(|| "parsing the fetched data")?;
    debug!("Received: {}", json_value);

    let settings = settings::from(&mut json_value, config)?;
    debug!("Settings: {:#?}", settings);

    for settings in settings {
        let stream = TcpStream::connect((settings.imap_server.clone(), settings.imap_port))
            .with_context(|| {
                format!(
                    "Failed connecting to IMAP server {}:{}",
                    settings.imap_server, settings.imap_port
                )
            })?;
        let tls = RustlsConnector::new_with_native_certs()
            .with_context(|| "Failed initializing TLS with native certs")?;
        let tlsstream = tls
            .connect(&settings.imap_server, stream)
            .with_context(|| {
                format!(
                    "Failed establishing TLS connection to IMAP server {}:{}",
                    settings.imap_server, settings.imap_port
                )
            })?;

        let client = imap::Client::new(tlsstream);
        let mut session = client
            .login(
                settings.imap_username.clone(),
                settings.imap_password.clone(),
            )
            .map_err(|e| e.0)
            .with_context(|| {
                format!(
                    "Failed authenticating as {} at IMAP server {}",
                    settings.imap_username, settings.imap_server
                )
            })?;

        session
            .select("INBOX")
            .with_context(|| "Failed opening INBOX")?;

        for uid in session
            .uid_search("UNSEEN")
            .with_context(|| "Failed searching for UNSEEN messages")?
            .into_iter()
        {
            // Connect to SMTP server before fetching a message so an error doesn't mark it as seen.
            let mut builder =
                mail_send::SmtpClientBuilder::new(settings.smtp_server.clone(), settings.smtp_port)
                    .allow_invalid_certs();
            if let (Some(username), Some(password)) = (
                settings.smtp_username.clone(),
                settings.smtp_password.clone(),
            ) {
                builder = builder.credentials(mail_send::Credentials::new(username, password));
            }
            let mut sender = builder.connect().await.with_context(|| {
                format!("Failed connecting to SMTP server {}", settings.smtp_server)
            })?;

            let messages = session
                .uid_fetch(uid.to_string(), "RFC822")
                .with_context(|| format!("Failed fetching message {}", uid))?;

            // RFC 3501 allows sending other responses that for the requested message. Find our message of interest.
            let received_message = messages.iter().find(|message| message.uid == Some(uid));

            let received_message = match received_message {
                Some(message) => message,
                None => {
                    println!(
                        "FETCH response didn't include requested message {}. Continuing ...",
                        uid
                    );
                    continue;
                }
            };

            let body = match received_message.body() {
                Some(body) => body,
                None => {
                    eprintln!("No body for message {}", uid);
                    continue;
                }
            };
            println!("Processing message {}", uid);

            if config.write_messages {
                let filename = format!("{}_in", uid);
                let mut file = File::create(&filename)
                    .with_context(|| format!("Creating file {}", filename))?;
                file.write_all(body)?;
            }

            // Message should be ASCII but parse as UTF-8 for two reasons:
            // 1. convenience, Rust's `std` has no `from_ascii`
            // 2. resilience, should any non-ASCII character be in there it's probably UTF-8
            let body = match from_utf8(body) {
                Ok(body) => body,
                Err(e) => {
                    eprintln!("Failed parsing body as UTF-8: {}", e);
                    continue;
                }
            };
            let mut lines = body.split_inclusive("\r\n").peekable();

            // Find adress which sent the email.
            let from = lines
                .clone()
                .find(|line| line.starts_with("From: "))
                .map(|line| {
                    let start = "From: ".len();
                    let end = line.len() - 2;
                    line[start..end].to_owned()
                });

            // Prevent looping mail from list.
            if from.as_ref() == Some(&settings.sender) {
                println!(
                    "Skipping message which is from our own sender address {}.",
                    settings.sender
                );
                continue;
            }

            let mut is_auto_submitted = false;

            // Build copy without headers that will be changed.
            let mut stripped_body = String::new();
            while let Some(header) = lines.next_if(|line| *line != "\r\n") {
                if header.starts_with("From: ")
                    || header.starts_with("Reply-To: ")
                    || header.starts_with("Sender: ")
                    || header.starts_with("To: ")
                {
                    continue;
                }

                if header.starts_with(DATE_HEADER) {
                    let date = match chrono::DateTime::parse_from_rfc2822(
                        &header[DATE_HEADER.len()..header.len() - 2],
                    ) {
                        Ok(date) => date,
                        Err(e) => {
                            eprintln!("{} failed to parse: {}", header, e);
                            chrono::Local::now().fixed_offset()
                        }
                    };

                    let now = chrono::Local::now().fixed_offset();

                    if date < now - config.unadjusted_age {
                        stripped_body.push_str(&format!("Date: {}\r\n", now.to_rfc2822()));
                        continue;
                    }
                }

                if header == "Auto-Submitted: auto-replied\r\n" {
                    is_auto_submitted = true;
                }

                stripped_body.push_str(header);
            }
            for body in lines {
                stripped_body.push_str(body);
            }

            let recipients: &Vec<String> = if is_auto_submitted {
                settings.auto_reply_recipients.as_ref()
            } else {
                settings.recipients.as_ref()
            };

            for to in recipients.iter() {
                if from.as_ref() == Some(to) {
                    // Skip sending mail back to original sender.
                    continue;
                }
                let mut new_body = format!("From: {}\r\nTo: {}\r\n", settings.sender, to);
                if let Some(from) = from.as_ref() {
                    new_body.push_str(&format!("Reply-To: {}\r\n", from));
                }
                new_body.push_str(&stripped_body);
                let new_body = new_body.as_bytes();

                if config.write_messages {
                    let filename = format!("{}_to_{}", uid, to);
                    let mut file = File::create(&filename)
                        .with_context(|| format!("Creating file {}", filename))?;
                    file.write_all(new_body)?;
                }

                let message = mail_send::smtp::message::Message::new(
                    settings.sender.clone(),
                    std::iter::once(to.to_owned()),
                    new_body,
                );

                sender
                    .send(message)
                    .await
                    .with_context(|| format!("Sending message to {}", to))?;
            }
        }
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    let _ = rustls::crypto::ring::default_provider().install_default();

    let config = config::read()?;
    debug!("Configuration: {:#?}", config);

    let mut interval = time::interval(config.interval);
    interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

    loop {
        interval.tick().await;

        if let Err(e) = run(&config).await {
            error!("{:?}", e);
        }
    }
}
