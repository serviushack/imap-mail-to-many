# imap-mail-to-many

Read mails from an IMAP inbox and forward them to a list of recipients.

This is *like* a mailing list software in that it offers:

* Configurable list of recipients to forward mails to
* Adjust mail headers to not break DMARC

It significantly differs from mailing list software because it:

* reads mails via IMAP
* has no subscribe functionality
* has no bounce management (yet)

## Building

    cargo build

## Usage

The settings for fetching and sending mails must be available via HTTP as JSON.
This can be setup in many ways.
A simple but working setup is demonstrated here.

The configuration can be a static file served via a simple server like [miniserve](https://github.com/svenstaro/miniserve):

```sh
cat <<EOD > config.json
[
    {
        "imap_server": "imap.example.com",
        "imap_username": "list@example.com",
        "imap_password": "password",
        "sender": "list@example.com",
        "recipients": [
            "recipient1@example.org",
            "recipient2@other-domain.net"
        ],
        "smtp_server": "smtp.example.com",
        "smtp_username": "list@example.com",
        "smtp_password": "password"
    }
]
EOD
miniserve config.json
```

The application can then be run like this:

```sh
export APP_FETCH_URL=http://localhost:8080
./target/debug/imap-mail-to-many
```

It will now fetch all unread mails from the IMAP server and send them out to the listed recipients.

Different HTTP methods and authentication are supported as well as a flexible processing of the JSON document.
See the [configuration](doc/configuration.md) documentation for details.

### with User Interface

A setup with a user interface to change the configuration is described [here](doc/Directus.md).


## Alternatives

* [docker-simple-mail-forwarder](https://github.com/huan/docker-simple-mail-forwarder) but [apparently](https://github.com/huan/docker-simple-mail-forwarder/issues/123) doesn't handle senders with DMARC / SPF.
