FROM rust:bookworm AS builder

WORKDIR /usr/src/myapp
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y ca-certificates && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/imap-mail-to-many /usr/local/bin/imap-mail-to-many
CMD ["imap-mail-to-many"]
