# Testing

For a full end-to-end test of the application an IMAP server and SMTP server are required.
The configuration must also be provided in some way.
All of this is prepared for quick and easy testing.

Start by launching all required services:

```sh
cd dev
docker compose --profile static up -d
```

Only TLS connections are supported by the application.
Collect all self-signed certificates into one file which becomes the trust store:

```sh
openssl s_client -showcerts -connect localhost:1993 </dev/null 2>/dev/null | openssl x509 >> certs.pem
openssl s_client -showcerts -connect localhost:1025 </dev/null 2>/dev/null | openssl x509 >> certs.pem
```

Set the environment variable so the application and other tools like `curl` trust the certificates:

```sh
export SSL_CERT_FILE=certs.pem
```

The IMAP server has no mails by default.
Add some using this script:

```sh
file=$(mktemp)
for i in $(seq 1 10); do
  cat <<EOD >$file
Subject: Testmail $i
From: original@example.org
To: list@example.com
Date: $(date -R)
Message-ID: $(uuidgen)@example.org

content
EOD
  curl -T $file 'imaps://user:pass@localhost:1993/inbox'
done
rm $file
```

By default those mails are marked as seen.
The application only processes unread messages so remove the 'seen' flag for all of them:

```sh
curl 'imaps://user:pass@localhost:1993/inbox' -X 'STORE 1:* -FLAGS (\Seen)'
```

The above command must be repeated after every run of the application.

The number of unread messages can be checked using:

```sh
curl 'imaps://user:pass@localhost:1993/inbox' -X 'STATUS INBOX (UNSEEN)'
```

Set the necessary environment variables for running the application:

```sh
export APP_FETCH_URL=http://localhost:8080
```

Now the application can be run:

```sh
../target/debug/imap-mail-to-many
```

All sent mails will be seen in Mailcrab's interface at http://localhost:1080

## Intercepting Communication

To better understand errors when fetching or sending mails it can help to see the raw traffic.
As TLS is always used a simple packet capture doesn't help.
To intercept the traffic a tool like [mitmproxy](https://mitmproxy.org/) can be used.
Run the tool in a separate shell:

```sh
mitmproxy --mode reverse:tls://127.0.0.1:1025/ -p 1026 -k
```

Trust the certificate and use the different port:

```sh
openssl s_client -showcerts -connect localhost:1026 </dev/null 2>/dev/null | openssl x509 >> certs.pem
sed -i 's/1025/1026/' config.json
```
